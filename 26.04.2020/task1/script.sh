#!/bin/bash
set -e
input_file=$1

if [[ $input_file == "" ]] 
then input_file="./urls.txt"
fi

function checkURL {
while read LINE
do
http_code=$(curl -s -o /dev/null -w "%{http_code}" "$LINE")
echo -en "$LINE" http_code is "$http_code\n"
if [[ "$http_code" =~ (4..|5..) ]] 
then
echo "$LINE" have bad http_code 
exit 0
fi
done < $input_file
}

checkURL

