### Task №1
## Description of task: 
Скрипт должен проходить по списку URL'ов и проверять, что они все "живые".

- список URL'ов лежит в файле /urls.txt
- "живой" - значит код ответа не 5XX или 4XX
- проверялка должна быть оформлена в виде функции bash, которая должна вызываться внутри скрипта
- функция должна принимать в качестве входного параметра путь к файлу с URL'ами.
- при любом "плохом" ответе от сервиса прерывать дальнейшую проверку.

## How to use script

By default script uses local file "urls.txt". You can make it and put your's url, then: 
```sh
script.sh
```
or you can use your file with urls, then:
```sh
script.sh your_file
```
